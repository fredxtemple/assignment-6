#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include "mersenne.h"
#include <vector>

using namespace std;

int N;
vector<double> x(N), a(N), b(N);
MersenneTwister prng(1000);
double w()
{ double T = prng()/4294967295.0;
return T; //used to change pdf
}

double f (vector<double> x)
{
return x[0]*x[1];
}
double Integral (double (*f)(double), double acc, int dim)
{ double p = 1.0
for(int s=0; s<N ; s++)

{ p = p * (a[s]-b[s]);
double integral1 = 0.0;
double integral2 = 0.0;

for( int j= 0 ; j < 200; j++)
{ for(int J=0;J<N;J++)
integral1 = integral2/200.0;

for( int k= 0 ; k < 200; k++)
{
for(int K=0;K<N;K++)
N = 200; //sets the initial number of samples used
{
 while (abs((integral2-integral1) > integral1*acc)) {
integral1 = 0.5*(integral1+integral2);
integral2 = 0.0;  
N = N*2; // increase the number of points by a factor of 2
for(int o=0; o<N; o++) {

for(int l=0;l<N;l++)
{ x[l] =a[l] +((a[l]-a[l])*w());
} 

integral2 = integral2 +(f(x)*p);;
}
integral2 = integral2/double(N);
cout << integral2 << endl;

}

integral2 = 0.5*(integral1+integral2);
std::cout << "The integral is: " << integral2 << endl;
return 0; }
}
}
}
}


int main() 
{
 
  double a, b, acc;
  int N;
  int dim;  
  do
  { cin.clear();
  cout << "What is your accuracy: ";
  cin >> acc;
} 
while (cin.bad() || acc < 0 || acc > 1);
	do
  { cin.clear();
  cout << "What is the size of the vector: ";
  cin >> N;
} 
while (cin.bad() || N < 0 || N > 1);
	do
  { cin.clear();
  cout << "What is the lower limit: ";
  cin >> a;
}
while (cin.bad() || a < 0 || a > 1);
	
	do
  { cin.clear();
   cout << "What is the upper limit ";
  cin >> b;
} 
while (cin.bad() || b < 0 || b > 1);
	do
  { cin.clear();
   cout << "What are the dimensions";
   cin >> dim;
} 
while (cin.bad() || dim < 0 || dim > 3); }




	